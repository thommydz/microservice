<?php
declare (strict_types = 1);
namespace Bdm\MicroServices\Tests;

use Mockery;
use PHPUnit\Framework\TestCase;
use \Bdm\MicroServices\Connection;

/**
 *  Test microservice
 *
 *
 *  @author michiel
 */
class UserServiceTest extends TestCase
{
    protected function setUp()
    {
        $this->userId      = ['user_id' => 75175];
        $request           = Mockery::mock('Illuminate\Http\Request');
        $data              = ['request' => $request];
        $this->userService = Connection::make('user', $data);
        $this->assertInstanceOf('Bdm\MicroServices\Services\UserService', $this->userService);
    }

    public function tearDown()
    {
        Mockery::close();
        $this->userService = null;
    }

    /* Test */
    public function testHasProductSubscription(): void
    {

        $boolean = $this->userService->hasProductSubscription($this->userId);
        $this->assertInternalType('boolean', $boolean);
    }

    /* Test */
    public function testAddProductSubscription(): void
    {
        $productSubscription = $this->userService->addProductSubscription($this->userId);
        $this->assertInstanceOf('\App\Models\V2\User\UserSubscription', $productSubscription);
    }

    /* Test */
    public function testCancelProductSubscription(): void
    {
        $userSubscription = $this->userService->cancelProductSubscription(1);
        $this->assertInstanceOf('\App\Models\V2\User\UserSubscription', $userSubscription);

        $this->assertTrue($userSubscription->end_date->isPast());
    }

    /* Test */
    public function testAddCredits(): void
    {
        $data = [
            'credit_bundle_id' => 4,
            'mutation'         => 500,
            $this->userId,
        ];

        $creditTransaction = $this->userService->addCredits($data);
        $this->assertInstanceOf('\App\Models\V2\User\UserCreditTransaction', $creditTransaction);

        $secondTransaction = $this->userService->addCredits($this->userId);

        $check = $secondTransaction->credits > $creditTransaction->credits;
        $this->assertTrue($check);
    }
}
