<?php

namespace Bdm\MicroServices\Services;

/**
 *  Microservice divider
 *
 *  @author michiel
 */
class PaymentService extends BaseService
{

    public function __construct()
    {
        //
    }

    /**
     * Get Payment Methods
     * @param Subscription $subscription
     * @return string
     */
    public function getPaymentMethods($subscriptions = false)
    {
        return $subscriptions->paymentMethods ?? false;
    }

    /**
     * Add a transaction via microservice.
     * @param array $data The data to create the transaction with.
     */
    public function addTransaction($data) : array
    {
        config(['auth.security' => 0]);
        try {
            $response = app('Dingo\Api\Dispatcher')->post(
                'payment/transactions',
                $data
            );
        } catch (Dingo\Api\Exception\InternalHttpException $e) {
            // We can get the response here to check the status code of the error or response body.
            $response = $e->getResponse();
        }
        config(['auth.security' => 1]);
        return $response;
    }
}
