<?php

namespace Bdm\MicroServices\Services;

/**
 *  Base service
 *
 *  @author michiel
 */
class BaseService
{

    /**  @Illuminate\Http\Request; the request forwarded between microservices */
    public $request;

  
    public function setRequest(\Illuminate\Http\Request $Request)
    {
        $this->request = $Request;
    }
}
