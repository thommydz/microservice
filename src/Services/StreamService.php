<?php

namespace Bdm\MicroServices\Services;

/**
 *  Stream microservice
 *
 *  @author pxi
 */
class StreamService extends BaseService
{

    public function __construct()
    {
        //
    }

    /**
     * Add a \App\Models\V1\Video\Stream
     *
     * @return stream
     */
    public function addStream($data) : array
    {
        try {
            config(['auth.security' => 0]);
            $response =  app('Dingo\Api\Dispatcher')->post(
                'stream',
                $data
            );
            config(['auth.security' => 1]);
        } catch (Dingo\Api\Exception\InternalHttpException $e) {
            // We can get the response here to check the status code of the error or response body.
            $response = $e->getResponse();
        }
        return $response;
    }

    /**
     * Add a \App\Models\V1\Video\PhoneNumberStream
     *
     * @return stream
     */
    public function addPhoneStream($data) : array
    {
        try {
            config(['auth.security' => 0]);
            $response =  app('Dingo\Api\Dispatcher')->post(
                'phone-stream',
                $data
            );
            config(['auth.security' => 1]);
        } catch (Dingo\Api\Exception\InternalHttpException $e) {
            // We can get the response here to check the status code of the error or response body.
            $response = $e->getResponse();
        }
        return $response;
    }
}
