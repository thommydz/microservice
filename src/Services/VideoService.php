<?php

namespace Bdm\MicroServices\Services;

/**
 *  Video microservice
 *
 *  @author michiel, pxi
 */
class VideoService extends BaseService
{

    public function __construct()
    {
        //
    }

    /**
     * Get a video 
     *
     * @return stream
     */
    public function get($data) : array
    {
        try {
            config(['auth.security' => 0]);
            $response =  app('Dingo\Api\Dispatcher')->get(
                'videos',
                $data
            );
            config(['auth.security' => 1]);
        } catch (Dingo\Api\Exception\InternalHttpException $e) {
            // We can get the response here to check the status code of the error or response body.
            $response = $e->getResponse();
        }
        return $response;
    }
}
