<?php

namespace Bdm\MicroServices\Services;

/**
 *  Microservice divider
 *
 *  @author michiel
 */
class UserService extends BaseService
{

    public function __construct()
    {
        //
    }

    /**
     *
     * Check if user has active subscriptions
     *
     * @param $user_id A string containing the unique user identifer
     *
     * @return string
     */
    public function getActiveProductSubscription($user_id = null)
    {

        $params = http_build_query(array_merge($this->request->all(), [
            'filter[active]' => 1,
            'include '       => 'subscription_details,subscriptions'
        ]));
        if (!$user_id) {
            $user_id = $this->request->get('user_id');
        }

        $url = 'users/' . $user_id . '/subscriptions?' . $params;

        try {
            config(['auth.security' => 0]);
            $subscriptions = app('Dingo\Api\Dispatcher')->get($url);
            config(['auth.security' => 1]);
        } catch (Dingo\Api\Exception\InternalHttpException $e) {
            // We can get the response here to check the status code of the error or response body.
            $subscriptions = $e->getResponse();
        }
        

        return $subscriptions;
    }

    /**
     *
     * Add a user active subscriptions
     *
     * @param array $data | $data['user_id'] required
     *
     * @return subscription
     */
    public function addProductSubscription($data)
    {
        if (!isset($data['user_id'])) {
            return false;
        }

        config(['auth.security' => 0]);

        $dispatcher   = app('Dingo\Api\Dispatcher');
        $subscription = $dispatcher->post('users/' . $data['user_id'] . '/subscriptions', $data);

        config(['auth.security' => 1]);

        return $subscription;
    }

    /**
     *
     * Check if user has active subscriptions
     *
     * @param array $data
     *
     * @return subscription
     */
    public function cancelProductSubscription($id)
    {
        config(['auth.security' => 0]);

        $dispatcher   = app('Dingo\Api\Dispatcher');
        $subscription = $dispatcher->delete('users/subscriptions/' . $id);

        config(['auth.security' => 1]);

        return $subscription;
    }

    /**
     *
     * Check if user has active subscriptions
     *
     * @param array $data
     *
     * @return subscription
     */
    public function addCredits($data)
    {
        if (!isset($data['user_id'])) {
            return false;
        }
        config(['auth.security' => 0]);

        $dispatcher        = app('Dingo\Api\Dispatcher');
        $creditTransaction = $dispatcher->post('users/' . $data['user_id'] . '/credits', $data);

        config(['auth.security' => 1]);

        return $creditTransaction;
    }

    /**
     *
     * Check if user has active subscriptions
     *
     * @param array $data
     *
     * @return subscription
     */
    public function addRentCredits($data)
    {
        if (!isset($data['user_id'])) {
            return false;
        }
        config(['auth.security' => 0]);

        $dispatcher        = app('Dingo\Api\Dispatcher');
        $creditTransaction = $dispatcher->post('users/' . $data['user_id'] . '/rent-credits', $data);

        config(['auth.security' => 1]);

        return $creditTransaction;
    }

    /**
     *
     * get User by id
     *
     * @param $id
     *
     * @return user
     */
    public function getUser($user_id)
    {
        config(['auth.security' => 0]);

        $dispatcher        = app('Dingo\Api\Dispatcher');
        $user = $dispatcher->get('users/' . $user_id);

        config(['auth.security' => 1]);

        return $user;
    }
}
