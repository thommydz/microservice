<?php

namespace Bdm\MicroServices\Services;

/**
 *  Stream microservice
 *
 *  @author pxi
 */
class StreamLogService extends BaseService
{

    public function __construct()
    {
        //
    }

    /**
     * Add to streamlog
     * @param Stream $Stream The newly created stream
     * @return stream
     */
    public function addStreamLog($data) : array
    {
        try {
            config(['auth.security' => 0]);
            $response =  app('Dingo\Api\Dispatcher')->post(
                'streamlog',
                $data
            );
            config(['auth.security' => 1]);
        } catch (Dingo\Api\Exception\InternalHttpException $e) {
            // We can get the response here to check the status code of the error or response body.
            $response = $e->getResponse();
        }
        return $response;
    }
}
