<?php

namespace Bdm\MicroServices\Services;

/**
 *  Mail microservice
 *
 *  @author michiel
 */
class MailService extends BaseService
{

    public function __construct()
    {
        //
    }


    /**
     * Sendmail
     *
     * @return void
     */
    public function send($data) : void
    {     
        config(['auth.security' => 0]);
       
        $dispatcher   = app('Dingo\Api\Dispatcher');
        $dispatcher->json($data)->post('mail');

        config(['auth.security' => 1]);
    }
}
