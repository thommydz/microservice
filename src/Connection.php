<?php
namespace Bdm\MicroServices;

use Bdm\MicroServices\Exceptions\InvalidArgumentException;

/**
 *  Microservice devider
 *
 *
 *  @author michiel
 */
final class Connection
{

    /**
     *   @param $service string.
     *   @param $data array to set data to service
     *   @return Microservice
     */
    public static function make($service, $data = false)
    {

        // We'll check to see if the service exists given string. ( Str::studly )
        $class = __NAMESPACE__ . '\Services\\' . ucfirst($service) . 'Service';

        if (!class_exists($class)) {
            throw new InvalidArgumentException("Service [$service] not supported.");
        }

        $service = new $class;

        // set data to service
        if ($data) {
            foreach ($data as $setter => $value) {
                $method = 'set' . ucfirst($setter);
                if (method_exists($service, $method)) {
                    $service->{$method}($value);
                } else {
                    throw new InvalidArgumentException("Method [$setter] not supported.");
                }

            }
        }

        return $service;
    }

}
